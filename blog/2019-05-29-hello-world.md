

---
slug: hello-world
title: PG Programme curriculum

tags: [pg, ug]
---
<!--
Welcome to this blog. This blog is created with [**Docusaurus 2 alpha**](https://v2.docusaurus.io/). -->


<b>UG Programme</b>

<p align="justify">

IIT Jammu started B. Tech. programme in the year 2016 with an initial intake of 30 students each in Computer Science and Engineering, Electrical Engineering and Mechanical Engineering discipline. In the year 2017, B. Tech. in Civil Engineering discipline started with an initial intake of 30 students. In the year 2018, B. Tech. in Chemical Engineering discipline started with an initial intake of 30 students. Starting with the 2019-20 session, B. Tech. in Materials Engineering was added with an initial intake of 30 students.

UG Programme
The following programmes are offered by IIT Jammu for undergraduate studies.

B.Tech. in Chemical Engineering
B.Tech. in Civil Engineering
B.Tech. in Computer Science and Engineering
B.Tech. in Electrical Engineering
B.Tech. in Materials Engineering
B.Tech. in Mechanical Engineering
Admission to UG Programme
The admission to these programmes is done through Joint Entrance Examination - Advanced (JEE-Advanced) rank and administered by the Joint Admission Board (JAB) through centralized counseling conducted by JoSSA at the all-India level. 

For any query related to UG admission, please write to us at ug [DOT] admission [AT] iitjammu [DOT] ac [DOT] in
IIT Jammu profile at JoSAA Portal
Seat Matrix 2019
Opening and Closing Ranks in the previous years
Director's Message to students
Director's Message on Anti-Ragging
General FAQs
Helpline Number - +911912570633​  


About Branch/Discipline change
There is no provision of branch change in IIT Jammu curriculum. It is due to the provision of interdisciplinary nature of newly devised curriculum with a much wider common core encompassing many engineering disciplines flowing over to almost 4 semesters and branch core subjects starting right in the second semester. Many other features like design and research component, humanities breadth with social relevance, and minor area options are available to suit the students in exploring different Engineering discipline. The curriculum template has been structured to be inter-disciplinary and hands-on in nature to cater to the rapidly changing scientific /technological needs of the global community.


About Minor Degree requirements
Institute has a course credit system for evaluations. There are specific credit requirements for the award of the degree. Apart from a major degree in the admitted discipline at the time of admission, a student may pursue a minor degree in available interdisciplinary areas. One typically needs to earn a certain number of credits for the minor degree. The student might contact the Associate Dean (Academics) for more details one s/he joins IIT Jammu. The complete criteria to earn a minor degree consists of academic as well as non-academics components. Once a student joins, there will be sessions where all the rules and regulations will be informed and discussed in detail for the comprehensive understanding.


About pre-paid mobile phones
Pre-paid mobiles of other states do not work in J&K. There are no public phone booths on the campus. Please bring a postpaid mobile or procure a SIM here with Aadhar of the student. Institute will provide a stall for SIM procurement on the day of registration.

Fee payment: Kindly use Internet banking or bring DDs as instructions given below under list of documents section. No cash is accepted. 

Reporting and Registration
Dear Student, 

Congratulations on your success in JEE-Advanced and being a part of the IIT system after a rigorous examination and counseling process. </p>


<!--truncate-->

TAbout pre-paid mobile phones
Pre-paid mobiles of other states do not work in J&K. There are no public phone booths on the campus. Please bring a postpaid mobile or procure a SIM here with Aadhar of the student. Institute will provide a stall for SIM procurement on the day of registration.

Fee payment: Kindly use Internet banking or bring DDs as instructions given below under list of documents section. No cash is accepted. 

Reporting and Registration
Dear Student, 

Congratulations on your success in JEE-Advanced and being a part of the IIT system after a rigorous examination and counseling process.
