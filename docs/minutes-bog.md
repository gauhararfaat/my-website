---
id: minutes-bog
title: Minutes
sidebar_label: Composition
slug: /
---

<!--You can write content using [GitHub-flavored Markdown syntax](https://github.github.com/gfm/).-->

<p align="justify">	 At the outset, the Chairman extended a very warm welcome to all the members
to make it convenient to attend the meeting in Institute’s campus at Jammu. He showed
his pleasure on physical progress in Infrastructure and showed his interest to complete
the running project at the earliest. In addition, Chairman took interest in knowing the
fund position and its utilization by the Institute in last Financial Year along with
Estimates for current and ensuing Financial Year. He briefed on object heads of the
Grant-in-Aid received from MHRD, New Delhi. He also briefed about HEFA funding
towards capital expenditure and its importance to the members of the Committee.
FC.3-1.0: To Confirm the Minutes of 2nd meeting of the Finance Committee held
on 18.12.2018. 
The members noted that the draft minutes of the 2nd meeting of the Finance
Committee were circulated on 04.01.2019 among the present members to consider and
give their comments, if any. No comments have since been received so far.
Resolved that the minutes of 2nd meeting of the Finance Committee held on
18.12.2018, as per Annexure in agenda item to be confirmed.

</p> 
