---
id: doc3
title: Acad Motto
---
<p align="justify">
Academic Flexibility, Focus on Hands-on Learning, Varied Pedagogy, Mandatory Industrial Interface & Transparent Continuous Evaluation System are the hallmarks of the Academic Process at IIT. Students are encouraged to break the mould and go beyond their disciplines, inter-disciplinary courses are highly encouraged. Students are provided the flexibility to go beyond the traditional course requirements and choose courses based on their interest and career choice. There is a strong focus on Laboratory & Project based learning
At IIT Jammu, we believe that every student needs to garner knowledge in multiple spheres which will give them an edge in the tough and competitive arena of Life.
</p>
