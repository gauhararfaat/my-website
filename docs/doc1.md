---
id: doc1
title: Composition
sidebar_label: Composition
slug: /
---

<!--You can write content using [GitHub-flavored Markdown syntax](https://github.github.com/gfm/).-->




<b> Senate</b>

<p>Prof. M. S. Gaur: Director IIT Jammu	Chairman </p>
<p>Prof. S. K. Koul, Deputy Director Strategy, Planning, Research and International Affairs	Member</p>
<p>Prof. S. N. Singh, Deputy Director Operations and Infrastructure	Member</p>
<p>Prof. Ashoke Kumar Sarkar , Dean  Academics	Member</p>
<p>Prof. Devesh C. Jinwala, Dean Faculty	Member</p>
<p>Prof. Chandan Ghosh, Dean Students Welfare	Member</p>
<p>Prof. Pradeep C Parameswaran, School of Basic Sciences, IIT Mandi	Member</p>
<p>Prof. Amit Prashant, Civil Engineering, IIT Gandhinagar	Member</p>
<p>Prof. (Ms.) Gayatri Menon, National Institute of Design, Ahmedabad	Member</p>
<p>Dr. Tapan Misra,  Sr. Advisor to Chairman ISRO	Member</p>
<p>Dr. (Ms.) Meenakshi Munshi, Scientist “G”	Member</p>
<p>Head, Chemistry	Member</p>
<p>Head, Civil Engineering	Member</p>
<p>Head, Computer Science & Engineering	Member</p>
<p>Head, Electrical Engineering	Member</p>
<p>Head, HSS	Member</p>
<p>Head, Mathematics 	Member</p>
<p>Head, Mechanical Engineering	Member</p>
<p>Head, Physics	Member</p>
<p>Professors (Appointed by IIT Jammu)	Member</p>
<p>Mr. Satyendra Singh, Student Representative	Member</p>
<p>Mr. Gaurav Bansal, Student Representative	Member</p>
<p>Mr. Mantavya Goyal, Student Representative	Member</p>
<p>Ms. Taijaswaini Agarwal, Student Representative	Member</p>
<p>r. Raj Kumar Manjhiwal, Officiating Registrar	Secretary</p>

